﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication17
{
    class Program
    {
        static void Main(){
            String _string = "It was many and many a year ago on a kingdom by the sea";
            String _format_string = "";
            String _SetupOn = "on";
            foreach (string word in _string.Split(' ')){
                if (word.ToLower().Contains(_SetupOn))
                    _format_string += ", ";

                _format_string += word + " ";
            }
            Console.WriteLine(_format_string);
            Console.ReadKey();

        }
    }
}
