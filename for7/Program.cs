﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            int Numb, A, B;
            do
            {
                do Console.Write("Введите A ");
                while (!int.TryParse(Console.ReadLine(), out A));
                do Console.Write("Введите B ");
                while (!int.TryParse(Console.ReadLine(), out B));
            } while (A > B);
            Numb = 0;
            for (int i = A; i <= B; i++)
                Numb += i;
            Console.WriteLine(Numb);
            Console.ReadLine();
        }
    }
}
