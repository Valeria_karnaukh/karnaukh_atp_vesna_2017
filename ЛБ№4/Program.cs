﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 0;
            double x;
            for (x = 1; x <= 4; x += 0.2)
            {
                sum += Math.Log10(4 * x + 8);
            }
            Console.Write("Sum1=");
            Console.WriteLine(sum);
            x = 1;
            sum = 0;
            while (x <= 4)
            {
                x += 0.2;
                sum += Math.Exp(-x) + Math.Sin(2 * x);
            }
            Console.Write("Sum2=");
            Console.WriteLine(sum);
            Console.ReadLine();

        }
    }
}
