﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            int rez = 0;
            int[,] mas =
            {
                {0, 1, 0 },
                {1, 0, 1 },
                {0, 1, 0 }
            };

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    rez += mas[i, j];
                }
            }
            Console.WriteLine("Sum:{0}", rez);
            Console.ReadLine();
        }
    }
}
