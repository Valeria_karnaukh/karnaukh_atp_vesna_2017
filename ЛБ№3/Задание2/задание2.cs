﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введіть число від 1 до 24");
            string myLanguage = Console.ReadLine();

            sw1(myLanguage);

            Console.ReadLine();
        }
      static void sw1(string s)
        {
            switch (s)
            {
                case "1":
                case "21":
                    Console.WriteLine("година");
                    break;
                case "2":
                case "3":
                case "4":
                case "22":
                case "23":
                case "24":
                    Console.WriteLine("години");
                    break;
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                case "10":
                case "11":
                case "12":
                case "13":
                case "14":
                case "15":
                case "16":
                case "17":
                case "18":
                case "19":
                case "20":
                    Console.WriteLine("годин");
                    break;
                default:
                    Console.WriteLine("Не вірне число");
                    break;
            }
        }
    }
}
