﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            double z, x, y;
            Console.WriteLine("Input z:");
            z = Convert.ToDouble(Console.ReadLine());
            if (z < 0)
            {
                x = 1 / (Math.Pow(Math.Cos(z), 2));
            }
            else if (0 <= z && z <= 8)
            {
                x = z - Math.Log10(Math.Abs(z));
            }
            else
            {
                if (z > 8)
                    x = Math.Pow(z, 3) + Math.Sin(z);
            }
            y = Math.Tan(x) - 4 * Math.Pow(x, 3);
            Console.Write("y=f(x): ");
            Console.WriteLine(y);
            Console.ReadLine();
        }
    }
}
