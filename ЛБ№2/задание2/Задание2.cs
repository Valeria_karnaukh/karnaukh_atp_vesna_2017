﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number: ");
            string s = Console.ReadLine();
            double x = Convert.ToDouble(s);
            x = 2 * x + 4 + Math.Pow(Math.Cos(x), 2) - Math.Log10(Math.Abs(x + 8));
            Console.Write("Result is: ");
            Console.WriteLine(x);
            Console.ReadLine();
        }
    }
}
