﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication95
{
    class Program
    {
        public static double A(double x)
        {
            return Math.Cos(x) + 2 * x;
        }

        public static double B(double x)
        {
            return Math.Pow(x, 4) - ((2 * x) / 5);
        }

        public static double C(double x)
        {
            return 2 * x - 5;
        }

        public static double H(double a, double b, double c)
        {
            return 4 * Math.Pow(a, 2) + 5 * Math.Pow(b, 2);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Vidod: ");
            double x = Convert.ToDouble(Console.ReadLine());
            double a = A(x);
            double b = B(x);
            double c = C(x);
            double h = H(a, b, c);
            Console.WriteLine("H() = {0}", h);
            Console.ReadLine();

        }
    }
}
